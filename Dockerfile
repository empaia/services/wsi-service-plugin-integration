ARG VERSION_WSI_SERVICE
FROM registry.gitlab.com/empaia/services/wsi-service:${VERSION_WSI_SERVICE}

ARG VERSION_ISYNTAX_PLUGIN
RUN pip3 install wsi-service-plugin-isyntax==${VERSION_ISYNTAX_PLUGIN} --user --extra-index-url https://gitlab.com/api/v4/projects/27987299/packages/pypi/simple

ARG VERSION_MIRAX_PLUGIN
RUN pip3 install wsi-service-plugin-mirax==${VERSION_MIRAX_PLUGIN} --user --extra-index-url https://gitlab.com/api/v4/projects/44960927/packages/pypi/simple
