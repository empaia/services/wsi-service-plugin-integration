# WSI Service Plugin Integration

## Add further plugins

Add the following lines to the `Dockerfile`, e.g. example for the iSyntax plugin:

``` Dockerfile
ARG VERSION_ISYNTAX_PLUGIN
RUN pip3 install wsi-service-plugin-isyntax==${VERSION_ISYNTAX_PLUGIN} --extra-index-url https://gitlab.com/api/v4/projects/27987299/packages/pypi/simple
```

and add the following line to the `version.json` file:

```json
    "VERSION_ISYNTAX_PLUGIN": "x.x.x"
```

and add the build-arg to the the docker build job:

```yml
--build-arg VERSION_ISYNTAX_PLUGIN=$VERSION_ISYNTAX_PLUGIN 
```

Optional: For automatic plugin updates adapt `version_update.py`

## Update to latest versions

The pipeline is run automatically once a week.
