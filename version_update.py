import json

import requests
import semantic_version
import toml


def get_current_version_from_poetry_pyproject_url(poetry_pyproject_url):
    with requests.get(poetry_pyproject_url) as response:
        pyproject = toml.loads(response.text)
    return pyproject["tool"]["poetry"]["version"]


def patch_version(old_version):
    new_version = str(semantic_version.Version(old_version).next_patch())
    return new_version


def write_json(data):
    with open("version.json", "w") as f:
        json.dump(data, f, indent=4)


def main():
    with open("version.json") as f:
        data = json.load(f)
        print("Old:\n", json.dumps(data, indent=4))
        data["VERSION"] = patch_version(data["VERSION"])
        data["VERSION_WSI_SERVICE"] = get_current_version_from_poetry_pyproject_url(
            "https://gitlab.com/empaia/services/wsi-service/-/raw/main/pyproject.toml"
        )
        data["VERSION_ISYNTAX_PLUGIN"] = get_current_version_from_poetry_pyproject_url(
            "https://gitlab.com/empaia/services/wsi-service-plugin-isyntax/-/raw/main/pyproject.toml"
        )
        data["VERSION_MIRAX_PLUGIN"] = get_current_version_from_poetry_pyproject_url(
            "https://gitlab.com/empaia/services/wsi-service-plugin-mirax/-/raw/main/pyproject.toml"
        )
        print("New:\n", json.dumps(data, indent=4))
        write_json(data)


if __name__ == "__main__":
    main()
