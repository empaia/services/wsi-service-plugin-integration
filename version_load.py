import json


def main():
    with open("version.json") as f:
        data = json.load(f)
    with open("version.env", "w") as f:
        for key in data:
            f.write(f"{key}={data[key]}\n")


if __name__ == "__main__":
    main()
